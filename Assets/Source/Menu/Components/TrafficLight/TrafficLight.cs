﻿namespace Developer.Component
{
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Controller for a Traffic Light.
    /// </summary>
    public sealed class TrafficLight : MonoBehaviour
    {
        /// <summary>
        /// The light representing Stop.
        /// </summary>
        public Image red = null;

        /// <summary>
        /// The light representing Caution.
        /// </summary>
        public Image yellow = null;

        /// <summary>
        /// The light representing Go.
        /// </summary>
        public Image green = null;

        /// <summary>
        /// Turns off all lights.
        /// </summary>
        public void SetState()
        {
            if (red == null || yellow == null || green == null) Debug.LogError($"TrafficLight - SetState(): TrafficLight has a null reference.");

            red.color = Color.gray;
            yellow.color = Color.gray;
            green.color = Color.gray;
        }

        /// <summary>
        /// Sets the selected light on, while turning the other lights off.
        /// </summary>
        /// <param name="light">The light to switch on.</param>
        public void SetState(Test.LightColor light)
        {
            if (red == null || yellow == null || green == null) Debug.LogError($"TrafficLight - SetState(light): TrafficLight has a null reference.");

            switch (light)
            {
                case Test.LightColor.Red:
                    red.color = Color.red;
                    yellow.color = Color.gray;
                    green.color = Color.gray;
                    break;
                case Test.LightColor.Yellow:
                    red.color = Color.gray;
                    yellow.color = Color.gray;
                    green.color = Color.gray;
                    break;
                case Test.LightColor.Green:
                    red.color = Color.gray;
                    yellow.color = Color.gray;
                    green.color = Color.green;
                    break;
                default:
                    Debug.LogError($"TrafficLight - SetState(light): Attempted to set state on non-existent index {light}.");
                    break;
            }
        }
    }
}