﻿namespace Developer.Screen
{
    using System.Threading.Tasks;

    /// <summary>
    /// Controller for the initial screen that displays while the app background loads.
    /// </summary>
    public sealed class LoadingScreen : Core.ScreenAbstract
    {
        private async void Start()
        {
            await Task.Delay(2000);
            _app.ChangeScreen<LightScreen>();
        }
    }
}