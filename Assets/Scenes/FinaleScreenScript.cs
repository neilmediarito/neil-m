﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class FinaleScreenScript : MonoBehaviour
{
    public Color color1 = Color.red;
    public Color color2 = Color.blue;
    public float duration = 3.0F;
    public Texture btnTexture;

    public Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
        cam.clearFlags = CameraClearFlags.SolidColor;
    }


    void Update()
    {
        float t = Mathf.PingPong(Time.time, duration) / duration;
        cam.backgroundColor = Color.Lerp(color1, color2, t);
    }


    public void LoadScene()
    {
        SceneManager.LoadScene(0); //Reload to main scene
    }

    void OnGUI()
    {
        if (!btnTexture)
        {
            Debug.LogError("Please assign a texture on the inspector");
            return;
        }


        if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 50), btnTexture))
        {
            SceneManager.LoadScene(0); //Reload to the scene
            Debug.Log("Clicked the button with an image");
        }


        if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 50), btnTexture))
            Debug.Log("Clicked the button with text");
        {
            //SceneManager.LoadScene(0); //Reload to main scene
        }
    }
}
 

