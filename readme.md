Cogniss Unity Developer Test

------------------------------------------

Expected Time for Completion: <1 Hour
Unity Version: 2018.3.4f1

It is recommend you have Unity set up for at least Android builds.

This test has three challenges within the same project. Please extract the project and attempt to solve the following challenges listed. If you encounter any bugs or issues in the project, you should attempt to resolve them.

------------------------------------------

Challenge 1:

When creating development builds on test devices, it can sometimes be inconvient to connect to a debugging tool to read error logs. The Reporter allows you to drag your finger in a circular motion to open up an in-scene log viewer, to help debug issues without connecting to another machine.

Without using a prefab, or modifying the scene file in any way, load a new Reporter into the scene at runtime before any screens show. There should be no warnings or errors generated.

------------------------------------------

Challenge 2:

Networking requires getting data that may not be immediately available at the time you request it, without freezing up the app as you wait for it. A library has been provided to you which requires working asynchronously to get data without freezing the display. You can double-check the networking is ocurring in the background by attempting to open the Reporter with the circular finger gesture while you load the data for this challenge.

In the LightScreen, connect to the traffic network using the Test library 100 times; each time you do, you need to turn on a certain set of light displays. If the returned number is a multiple of 3, turn on the Red light. If the returned number is a multiple of 5, turn on the yellow light. If it is a multiple of both 3 and 5, turn on the green light. If it not a multiple of either, then no lights should be on. The lights should remain on for a minimum of 3 seconds after each switch.

Extra Credit: Add a text field that displays a count-down to the next light switch. Do not use the Inspector to link this field to the script.

------------------------------------------

Challege 3:

Multiplatform application developers often need to make changes depending on the specific platform, without maintaining separate project folders.

Finish the FinaleScreen so that the background is a solid colour, and it contains a single button justified in the centre in both Landscape and Portrait. If the platform is Android, the background colour should be red, if the platform is IOS, the background colour should be green, and if the platform is the editor, the background colour should be green. The button should restart the app from the beginning.